import { getDetails } from "lib/graphql/queryHelper";
import useSWR from "swr";

export default function useDetails(id) {
  const key = `details_${id}`;
  const { data, error, mutate } = useSWR(key, () => id ? getDetails(id) : null);

  return {
    data,
    isLoading: !data && !error,
    isError: error,
    mutate
  }
}
