import { getTypes } from "lib/graphql/queryHelper";
import Item from "models/Item";
import useSWR from "swr";

export default function useTypes() {
  const key = "types";
  const { data, error } = useSWR(key, () => getTypes());

  return {
    types: data,
    isError: error,
  }
}
