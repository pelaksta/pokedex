import React, { useEffect } from "react";

// https://stackoverflow.com/questions/32553158/detect-click-outside-react-component
export default function useBlurAlert(ref: React.MutableRefObject<any>, active: boolean, callback: () => void) {
  useEffect(() => {
    function handleClickOutside(event: MouseEvent) {
      if (active && ref.current && !ref.current.contains(event.target)) {
        callback();
      }
    }

    // Bind the event listener
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref, active, callback]);
}
