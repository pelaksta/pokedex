import useSWR from "swr";
import { getPokemons } from "lib/graphql/queryHelper";
import useDebounce from "./useDebounce";

export default function useItems(pageIdx: number, size: number, searchQuery?: string, type?: string, favorited?: boolean) {
  const query = useDebounce(searchQuery, 300);

  const key = [favorited ? "fav" : "", "items", query, type, pageIdx, size].join("/");
  const { data, error, mutate } = useSWR(key, () => getPokemons(pageIdx, size, query, type, favorited));

  return {
    items: data,
    isLoading: !data && !error,
    isError: error,
    mutate
  }

}
