import { useMutation } from "@apollo/client";
import { FAVORITE_QUERY, UNFAVORITE_QUERY } from "lib/graphql/queryHelper";
import client from "../lib/graphql/client";

export default function useMutations(mutateCallback?: () => void) {

  const nukeFavResults = (cache) => {
    cache.modify({
      fields: { pokemons : (current, details) =>
        (details.storeFieldName.includes("isFavorite"))
        ? details.DELETE
        : current
      },
    });
  }

  const [favorite, { error: favError }] = useMutation(FAVORITE_QUERY, {
    client,
    update(cache, { data: { favoritePokemon } }) {
      cache.modify({
        id: cache.identify(favoritePokemon),
        fields: { isFavorite() { return true; } },
      });
      nukeFavResults(cache);
      mutateCallback?.();
    }
  });

  const [unfavorite, { error: unfavError }] = useMutation(UNFAVORITE_QUERY, {
    client,
    update(cache, { data: { unFavoritePokemon } }) {
      const id = cache.identify(unFavoritePokemon);
      cache.modify({
        id,
        fields: { isFavorite : () => false },
      });

      cache.modify({
        fields: { pokemons : (current, details) =>
          (details.storeFieldName.includes("isFavorite")) // Nuke all cached fav search results
          ? details.DELETE
          : current
        },
      });
      nukeFavResults(cache);
      mutateCallback?.();
    }
  });

  return {
    hasError: favError || unfavError,
    favorite: (id: string) => favorite({ variables: { id } }),
    unfavorite: (id: string) => unfavorite({ variables: { id } })
  }

}
