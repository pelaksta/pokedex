# Pokedex example

## Install
Run `yarn` or `npm i`

## Configure
GraphQL endpoint is configured using `.env(*)` files - the default configuration for DEV purposes is in `.env.development`.
For more details see [here](https://nextjs.org/docs/basic-features/environment-variables);

## Run
Run `yarn dev` or `npm start dev`

## Test
TODO