type SearchParams = {
  favorited?: boolean;
  pageIdx?: number;
  query?: string;
  type?: string;
};

export default SearchParams;