type Item = {
  id: string;
  name: string;
  types: string[];
  image: string;
  isFavorite: boolean;
}

export default Item;

type Dimension = {
  minimum: number;
  maximum: number;
}

export type ItemWithDetails = Item & {
  evolutions: Item[];
  height: Dimension;
  maxCP: number;
  maxHP: number;
  sound: string;
  weight: Dimension;
};