import { gql } from "@apollo/client";
import Item, { ItemWithDetails } from "models/Item";
import client from "./client";

export type FavoriteResult = {
  id: string;
  isFavorite: boolean;
}

export const getPokemons = async (pageIdx = 0, size: number, searchQuery?: string, type?: string, favorited?: boolean): Promise<Item[]> => {
  const filters = [
    type ? `type: "${type}"` : undefined,
    favorited ? "isFavorite: true" : undefined,
  ].filter(Boolean).join(", ")

  const query = [
    `limit: ${size}`,
    `offset: ${pageIdx * size}`,
    searchQuery ? `search: "${searchQuery}"` : undefined,
    type || favorited ? `filter: { ${filters} }` : undefined
  ].filter(Boolean).join(", ");

  const { data } = await client.query({
    query: gql`
      query {
        pokemons(
          query: {
            ${query}
          }
        ) {
          edges {
            id,
            name,
            types,
            image,
            isFavorite
          }
        }
      }
    `,
    // variables: {
    //   limit: size,
    //   offset: pageIdx * size,
    //   searchQuery,

    // }
  });

  return data?.pokemons?.edges;
}

export const getTypes = async (): Promise<string[]> => {
  const { data } = await client.query({
      query: gql`
        query {
          pokemonTypes
        }
      `,
    });

  return data?.pokemonTypes;
}

export const getDetails = async (id: string): Promise<ItemWithDetails> => {
  const { data } = await client.query({
      query: gql`
        query getDetails($id: ID!) {
          pokemonById(id: $id)
          {
            id,
            name,
            types,
            image,
            isFavorite,
            evolutions {
              id,
              name,
              isFavorite,
              image
            },
            height { minimum, maximum },
            maxCP,
            maxHP,
            sound,
            weight { minimum, maximum }
          }
        }
      `,
      variables: { id }
    });

  return data?.pokemonById;
}

export const FAVORITE_QUERY = gql`
  mutation Favorite($id: ID!) {
    favoritePokemon(id: $id)
    { id, isFavorite }
  }
`;

export const UNFAVORITE_QUERY = gql`
  mutation Favorite($id: ID!) {
    unFavoritePokemon(id: $id)
    { id, isFavorite }
  }
`;
