import classNames from "classnames/bind";
import React from "react";

import styles from "./Notification.module.scss";
const cx = classNames.bind(styles);

export enum NotificationType {
  ERROR = "error",
  SUCCESS = "success"
}

type NotificationState = {
  type: NotificationType | null;
  message: string | null;
  autoCloseTimeout: NodeJS.Timeout | null;
}

const initialState: NotificationState = {
  type: null,
  message: null,
  autoCloseTimeout: null
}

export default class Notification extends React.Component<unknown, NotificationState> {
  state = initialState

  show = (type: NotificationType, message: string, autoCloseMs = 1000) => {
    clearTimeout(this.state.autoCloseTimeout);

    const autoCloseTimeout = autoCloseMs ? setTimeout(this.close, autoCloseMs) : null;
    this.setState({ type, message, autoCloseTimeout });
  }

  close = () => {
    clearTimeout(this.state.autoCloseTimeout);
    this.setState(initialState);
  }

  render = () => {
    const { type, message } = this.state;

    return (
      <div className={cx("notification", `notification--${type}`, { open: message })}>{message}</div>
    );
  }
}