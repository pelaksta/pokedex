import { useCallback } from "react";
import LikeButton from "components/IconButton/LikeButton";
import Item from "models/Item"
import classNames from "classnames/bind";

import styles from "./ItemCard.module.scss"
import Link from "next/link";
import { GalleryViewMode } from "components/Gallery";
const cx = classNames.bind(styles);

type ItemCardProps = {
  item: Item;
  mode: GalleryViewMode;
  toggleFavourite?: (itemId: string, toggle: boolean) => void;
}

export default function ItemCard({
  item,
  mode = GalleryViewMode.TILES,
  toggleFavourite
}: ItemCardProps) {
  const { id, isFavorite, name, image, types } = item;
  const toggleCallback = useCallback(() => toggleFavourite?.(id, !isFavorite), [id, isFavorite, toggleFavourite]);

  return (
    <div className={cx("item-card", `item-card--${mode}`)}>
      <div className={cx("item-card__image")}>
        <Link href={`/pokemon/${item.id}`}>
          <a>
            <img src={image} alt={name} /> {/* eslint-disable-line @next/next/no-img-element */}
          </a>
        </Link>
      </div>
      <div className={cx("item-card__info")}>
        <div>
          <div className={cx("item-card__info__name")}>{ name }</div>
          <div className={cx("item-card__info__types")}>{ types?.join(", ") }</div>
        </div>
        <div className={cx("item-card__actions")}>
          <LikeButton onToggle={toggleCallback} toggled={isFavorite}/>
        </div>
      </div>
    </div>
  )
}