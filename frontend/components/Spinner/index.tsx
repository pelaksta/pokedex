import classnames from "classnames/bind";
import styles from "./Spinner.module.scss"
const cx = classnames.bind(styles);

export default function Spinner() {
  return (
    <div className={cx("spinner")}>
      <div className={cx("shadow")}></div>

      <div className={cx("ball")}>
        <div className={cx("ball__upper")}></div>
        <div className={cx("ball__button")}></div>
      </div>
    </div>
  );
}