import Gallery, { GalleryViewMode } from "../Gallery";
import Notification, { NotificationType } from "../Notification";
import useItems from "../../hooks/useItems";
import { useCallback, useEffect, useRef } from "react";
import GalleryHeader from "components/GalleryHeader";
import FavoritesToggle from "components/FavoritesToggle";
import useTypes from "hooks/useTypes";
import LoadingOverlay from "components/LoadingOverlay";
import classnames from "classnames/bind";

import styles from "./GalleryPage.module.scss";
import SearchParams from "models/SearchParams";
import useMutations from "hooks/useMutations";
const cx = classnames.bind(styles);

type GalleryPageProps = {
  onSearchParamsChange: (searchParams: Partial<SearchParams>) => void;
  onViewModeChange: (viewMode: GalleryViewMode) => void;
  searchParams: SearchParams;
  viewMode: GalleryViewMode;
}

export default function GalleryPage({
  onSearchParamsChange,
  onViewModeChange,
  searchParams: { favorited, pageIdx = 0, query, type },
  viewMode
}: GalleryPageProps) {
  const pageSize = 20;
  const notificationRef = useRef<Notification>();

  const { items, isError, isLoading, mutate } = useItems(pageIdx, pageSize, query, type, favorited);
  const { favorite, unfavorite, hasError: mutationsError } = useMutations(mutate);
  const { types, isError: typesError } = useTypes();

  // Notification
  useEffect(() => {
    (isError || typesError || mutationsError) && notificationRef.current?.show(NotificationType.ERROR, "Fetching the data failed" );
  }, [isError, typesError, mutationsError]);

  const handleToggleFavouriteItem = useCallback(async (id: string, toggle: boolean) => {
    try {
      toggle ? favorite(id) : unfavorite(id);
      notificationRef.current?.show(NotificationType.SUCCESS, "Changes saved");
    } catch (e) {
      console.error(e);
      notificationRef.current?.show(NotificationType.ERROR, "Your selection couldn't be saved");
    }
  }, [favorite, unfavorite]);

  const toggleViewFavorites = useCallback((favorited: boolean) => onSearchParamsChange({ favorited }), [onSearchParamsChange]);

  const handleTypeFilter = useCallback((type: string) => onSearchParamsChange({ type }), [onSearchParamsChange]);

  const handleSearch = useCallback((query: string) => onSearchParamsChange({ query}), [onSearchParamsChange]);

  const handleNextPage = useCallback(() => onSearchParamsChange({ pageIdx: pageIdx + 1 }), [onSearchParamsChange, pageIdx]);

  const handlePrevPage = useCallback(() => onSearchParamsChange({ pageIdx: pageIdx - 1 }), [onSearchParamsChange, pageIdx]);

  return (
    <div className={cx("gallery-page")}>
      <Notification ref={notificationRef} />

      <header className={cx("gallery-page__header")}>
        <FavoritesToggle favoritesToggled={favorited} onToggleFavorites={toggleViewFavorites} />

        <GalleryHeader
          onChangeView={onViewModeChange}
          onFilterByType={handleTypeFilter}
          onSearchByQuery={handleSearch}
          types={types}
          typeFilter={type}
          searchQuery={query}
        />
      </header>

      <div>
        {
          isLoading
          ? <LoadingOverlay />
          : <Gallery
              pageSize={pageSize}
              items={items}
              toggleFavourite={handleToggleFavouriteItem}
              onNextPage={handleNextPage}
              onPreviousPage={pageIdx > 0 ? handlePrevPage : undefined}
              viewMode={viewMode}
            />
        }
      </div>
    </div>
  )
}
