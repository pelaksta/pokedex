import { ItemWithDetails } from "models/Item";
import classnames from "classnames/bind";
import styles from "./DetailsCard.module.scss";
import IconButton from "components/IconButton";
import { useCallback } from "react";
import ItemCard from "components/ItemCard";
import LikeButton from "components/IconButton/LikeButton";
import ProgressBar from "components/ProgressBar";
import Speaker from "components/Icons/Speaker";

const cx = classnames.bind(styles);

type DetailsCardProps = {
  item: ItemWithDetails;
  onToggleFavorite: (id: string, toggle: boolean) => void;
};

const Dimensions = ({ min, max, label }: { min: number, max: number, label: string }) => (
  <div className={cx("dimension")}>
    <div className={cx("dimension__label")}>{label}</div>
    <div className={cx("dimension__value")}>{min} - {max}</div>
  </div>
)

export default function DetailsCard({
  item: {
    id,
    image,
    name,
    types,
    weight,
    height,
    sound,
    evolutions,
    isFavorite,
    maxCP,
    maxHP
  },
  onToggleFavorite
}: DetailsCardProps) {
  const playSound = useCallback(() => (new Audio(sound)).play(), [sound]);

  const toggleFavoriteItem = useCallback(() => onToggleFavorite(id, !isFavorite), [id, isFavorite, onToggleFavorite]);

  return (
    <div className={cx("container")}>
      <div className={cx("details")}>
        <div className={cx("details__image")}>
          <img src={image} alt={name} /> {/* eslint-disable-line @next/next/no-img-element */}
          {sound && <IconButton onToggle={playSound}>
            <Speaker />
          </IconButton>}
        </div>
        <div className={cx("details__info")}>
          <div>
            <div className={cx("details__info__name")}>{name}</div>
            <div className={cx("details__info__types")}>{types.join(", ")}</div>
          </div>
          <LikeButton onToggle={toggleFavoriteItem} toggled={isFavorite}/>
        </div>
        <table className={cx("details__sliders")}>
          <tbody>
            <tr>
              <td><ProgressBar max={maxCP} value={maxCP} color="#71C1A1" /></td>
              <td className={cx("slider__label")}>CP: {maxCP}</td>
            </tr>
            <tr>
              <td><ProgressBar max={maxHP} value={maxHP} color="#9F9FFE" /></td>
              <td className={cx("slider__label")}>HP: {maxHP}</td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className={cx("details__dimensions")}>
        <Dimensions label="Weight" min={weight.minimum} max={weight.maximum} />
        <Dimensions label="Height" min={height.minimum} max={height.maximum} />
      </div>

      { evolutions.length > 0 &&
        <div className={cx("evolutions")}>
          <h2>Evolutions</h2>
          <div className={cx("evolutions__list")}>
            {evolutions.map((evolution) => <ItemCard key={evolution.id} item={evolution} toggleFavourite={onToggleFavorite} />)}
          </div>
        </div>
      }
    </div>
  );
}