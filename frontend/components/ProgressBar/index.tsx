import classNames from "classnames/bind";
import styles from "./ProgressBar.module.scss";
const cx = classNames.bind(styles);

export type ProgressBarProps = {
  value: number,
  max: number,
  color: string
};

export default function ProgressBar({ value, max, color }: ProgressBarProps) {
  const progress = value > max ? 100 : Math.floor(value / max * 100);

  return (
    <div className={cx("progress")}>
      <div style={{ width: `${progress}%`, backgroundColor: color }} className={cx("progress__bar")}></div>
    </div>
  );
}