export default function Rows() {
  return (
    <svg width="1200px" height="1200px" viewBox="0 0 1200 1200" enableBackground="new 0 0 1200 1200">
      <path d="M0,0v240h1200V0H0z M0,480v240h1200V480H0z M0,960v240h1200V960H0z"/>
    </svg>
  );
}