export default function Heart() {
  return (
    <svg fill="none" height="24" stroke="black" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" viewBox="0 0 24 24" width="24">
      <path d="M 20.8 4.6 a 5.5 5.5 0 0 0 -7.8 0 L 12 5.7 l -1.1 -1.1 a 5.5 5.5 0 0 0 -7.8 7.8 l 1.1 1.1 L 12 21.2 l 7.8 -7.8 l 1.1 -1.1 a 5.5 5.5 0 0 0 0 -7.8 z"/>
    </svg>
  );
}