export default function Tiles() {
  return (
    <svg viewBox="0 0 278 278">
      <g>
        <rect x="0" y="0" width="119.054" height="119.054"/>
        <rect x="158.946" y="0" width="119.054" height="119.054"/>
        <rect x="158.946" y="158.946" width="119.054" height="119.054"/>
        <rect x="0" y="158.946" width="119.054" height="119.054"/>
      </g>
    </svg>
  );
}