import classNames from "classnames/bind";

import styles from "./Button.module.scss";
const cx = classNames.bind(styles);

export type ButtonProps = {
  children?: React.ReactNode;
  onClick: () => void;
}

export default function Button({
  children,
  onClick
}: ButtonProps) {

  return (
    <button className={cx("btn")} onClick={onClick}>
      {children}
    </button>
  );
}