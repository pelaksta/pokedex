import "react";
import Item from "models/Item"
import ItemCard from "../ItemCard";
import Button from "components/Button";

import styles from "./Gallery.module.scss";
import classnames from "classnames/bind";

const cx = classnames.bind(styles);

export enum GalleryViewMode {
  TILES = "tiles",
  LIST = "list"
}

type GalleryProps = {
  items?: Item[];
  onNextPage?: () => void;
  onPreviousPage?: () => void;
  pageSize?: number;
  toggleFavourite?: (itemId: string, toggle: boolean) => void;
  viewMode?: GalleryViewMode;
}

export default function Gallery({
  items,
  onNextPage,
  onPreviousPage,
  pageSize,
  toggleFavourite,
  viewMode = GalleryViewMode.TILES
}: GalleryProps) {
  const isLastPage = (items || []).length < pageSize;

  return (
    <div className={cx("gallery", `gallery--${viewMode}`)}>
      <div className={cx("gallery__content")}>
        {items?.map((item) => (
          <ItemCard key={item.id} item={item} toggleFavourite={toggleFavourite} mode={viewMode} />
        ))}
        {!items?.length && <p className={cx("gallery__placeholder")}>Nothing found :(</p>}
      </div>
      <div className={cx("gallery__pagination")}>
        {onPreviousPage && <Button onClick={onPreviousPage}>Previous</Button>}
        {onNextPage && !isLastPage && <Button onClick={onNextPage}>Next</Button>}
      </div>
    </div>
  )
}