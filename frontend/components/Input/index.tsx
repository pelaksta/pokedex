import React, { ChangeEventHandler, useCallback } from "react";
import classNames from "classnames/bind";

import styles from "./Input.module.scss";
const cx = classNames.bind(styles);

type InputProps = Omit<React.InputHTMLAttributes<HTMLInputElement>, "onChange"> & {
  onChange: (value: string) => void;
}

export default function Input({
  onChange,
  ...inheritedProps
}: InputProps) {
  const inputChangeHandler = useCallback<ChangeEventHandler<HTMLInputElement>>(
    (event) => {
      onChange(event.target.value);
    },
    [onChange]
  );

  return (
    <input className={cx("input")} {...inheritedProps} onChange={inputChangeHandler} />
  );
}
