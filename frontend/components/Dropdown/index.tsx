import React, { useCallback, useRef, useState } from "react";
import classNames from "classnames/bind";

import styles from "./Dropdown.module.scss";
import useBlurAlert from "hooks/useBlurAlert";
const cx = classNames.bind(styles);

type DropdownProps = {
  onChange: (value: string) => void;
  options: Array<{label?: string; value: string}>;
  placeholder?: string;
  value?: string;
}

type ListOptionProps = {
  className?: string;
  children?: React.ReactNode;
  value: string;
  onClick: (value: string) => void;
}

const ListOption = ({ className, children, value, onClick }: ListOptionProps) => (
  <div className={cx("dropdown-option", className)} onClick={() => onClick(value)}>{children}</div>
);

export default function Dropdown({
  onChange,
  options,
  placeholder,
  value
}: DropdownProps) {
  const wrapperRef = useRef();
  const [open, setOpen] = useState<boolean>(false);

  const toggleList = useCallback(() => setOpen(!open), [open]);
  const close = useCallback(() => setOpen(false), []);
  const dropdownChangeHandler = useCallback(
    (value) => {
      close();
      onChange(value);
    },
    [onChange, close]
  );

  useBlurAlert(wrapperRef, open, close);

  const selectedOption = options.find(o => o.value === value);

  return (
    <div ref={wrapperRef} className={cx("dropdown", { open, unset: !value })}>
      <div className={cx("dropdown__field")} onClick={toggleList}>
        <span>{ selectedOption === undefined ? placeholder : (selectedOption.label || selectedOption.value) }</span>
        <span className={cx("caret")}></span>
      </div>
      <div className={cx("dropdown__list")}>
        <ListOption className={cx("dropdown-option--reset")} value="" onClick={dropdownChangeHandler}>None</ListOption>
        {options.map((option) =>
          <ListOption
            key={option.value}
            className={cx({selected: option.value === value})}
            value={option.value}
            onClick={dropdownChangeHandler}
          >{option.label || option.value}</ListOption>
        )}
      </div>
    </div>
  );
}
