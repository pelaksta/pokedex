import classnames from "classnames/bind";
import Spinner from "components/Spinner";
import styles from "./LoadingOverlay.module.scss"
const cx = classnames.bind(styles);

export default function LoadingOverlay() {
  return (
    <div className={cx("loading-overlay")}>
      <div className={cx("spinner")}><Spinner /></div>
    </div>
  );
}