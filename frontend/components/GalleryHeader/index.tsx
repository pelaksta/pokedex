import styles from "./GalleryHeader.module.scss";
import classnames from "classnames/bind";
import { GalleryViewMode } from "components/Gallery";
import Input from "components/Input";
import Dropdown from "components/Dropdown";
import { useCallback, useMemo } from "react";
import IconButton from "components/IconButton";
import Tiles from "components/Icons/Tiles";
import Rows from "components/Icons/Rows";

const cx = classnames.bind(styles);

type GalleryHeaderProps = {
  onChangeView: (mode: GalleryViewMode) => void;
  onFilterByType: (type: string) => void;
  onSearchByQuery: (query: string) => void;
  searchQuery?: string;
  typeFilter?: string;
  types?: string[];
}

export default function GalleryHeader({
  onChangeView,
  onFilterByType,
  onSearchByQuery,
  searchQuery = "",
  typeFilter,
  types
}: GalleryHeaderProps) {
  const allTypes = useMemo(() => (types || []).map(value => ({ value })), [types]);

  const toggleTilesView = useCallback(() => onChangeView(GalleryViewMode.TILES), [onChangeView]);
  const toggleListView = useCallback(() => onChangeView(GalleryViewMode.LIST), [onChangeView]);

  return (
    <div className={cx("gallery-header")}>
      <Input type="text" placeholder="Search" value={searchQuery} onChange={onSearchByQuery} />

      <Dropdown placeholder="Type" value={typeFilter} onChange={onFilterByType} options={allTypes} />

      <div className={cx("view-mode")}>
        <IconButton toggled={false} onToggle={toggleTilesView}>
          <Tiles />
        </IconButton>

        <div className={cx("view-mode__separator")}></div>

        <IconButton toggled={false} onToggle={toggleListView}>
          <Rows />
        </IconButton>
      </div>
    </div>
  )
}