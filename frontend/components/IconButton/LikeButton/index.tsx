import Heart from "components/Icons/Heart";
import classNames from "classnames/bind";

import styles from "./LikeButton.module.scss";
const cx = classNames.bind(styles);

export type LikeButtonProps = {
  toggled: boolean;
  onToggle: () => void;
}

export default function LikeButton({
  onToggle,
  toggled
}: LikeButtonProps) {

  return (
    <button className={cx("btn", { "btn--toggled": toggled })} onClick={onToggle}>
      <Heart />
    </button>
  );
}