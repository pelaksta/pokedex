import classNames from "classnames/bind";

import styles from "./IconButton.module.scss";
import React from "react";
const cx = classNames.bind(styles);

export type IconButtonProps = {
  children?: React.ReactNode;
  toggled?: boolean;
  onToggle: () => void;
}

export default function IconButton({
  children,
  onToggle,
  toggled
}: IconButtonProps) {

  return (
    <button className={cx("btn", { "btn--toggled": toggled })} onClick={onToggle}>
      {children}
    </button>
  );
}