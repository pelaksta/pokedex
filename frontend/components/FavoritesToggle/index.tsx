import classNames from "classnames/bind";
import { useCallback } from "react";

import styles from "./FavoritesToggle.module.scss";
const cx = classNames.bind(styles);

type FavoritesToggleProps = {
  favoritesToggled: boolean;
  onToggleFavorites: (favorites: boolean) => void;
}

export default function FavoritesToggle({ favoritesToggled, onToggleFavorites }: FavoritesToggleProps) {
  const toggleAll = useCallback(() => onToggleFavorites(false), [onToggleFavorites]);
  const toggleFavs = useCallback(() => onToggleFavorites(true), [onToggleFavorites]);

  return (
    <div className={cx("wrapper")}>
      <button className={cx({ toggled: !favoritesToggled })} onClick={toggleAll}>All</button>
      <button className={cx({ toggled: favoritesToggled })} onClick={toggleFavs}>Favorites</button>
    </div>
  );
}
