import Head from "next/head";
import { useCallback, useEffect, useState } from "react";

import styles from "../styles/Home.module.css";
import { useRouter } from "next/router";
import GalleryPage from "components/GalleryPage";
import SearchParams from "models/SearchParams";
import { ParsedUrlQuery } from "querystring";
import { GalleryViewMode } from "../components/Gallery";

const viewModeStorageKey = "viewmode";

const parseQueryParams = (queryParams: ParsedUrlQuery): SearchParams => ({
  ...queryParams,
  favorited: queryParams.favorited === "true",
  pageIdx: queryParams.pageIdx ? Number.parseInt(queryParams.pageIdx as string) : undefined,
})

export default function Home() {
  const router = useRouter();
  const [viewMode, setViewMode] = useState(null);

  // Rehydrate view mode
  useEffect(() => {
    const storedViewMode = localStorage.getItem(viewModeStorageKey);
    setViewMode(storedViewMode || GalleryViewMode.TILES);
  }, []);

  // Manage local storage
  useEffect(() => {
    const storedViewMode = localStorage.getItem(viewModeStorageKey);
    if (storedViewMode !== viewMode) {
      localStorage.setItem(viewModeStorageKey, viewMode);
    }
  }, [viewMode]);

  const updateQueryParams = useCallback((searchParams: SearchParams) => {
    router.push({ pathname: router.pathname, query: searchParams });
  }, [router]);

  const handleSearchParamsChange = useCallback((change: Partial<SearchParams>) => {
    const changedKeys = Object.keys(change);
    const nextParams = (changedKeys.length === 1 && changedKeys[0] === "pageIdx")
      ? { ...router.query || {}, ...change }
      : { ...router.query || {}, ...change, pageIdx: 0 };

    updateQueryParams(nextParams);
  }, [router.query, updateQueryParams]);

  const handleViewModeChange = useCallback(setViewMode, [setViewMode]);

  return (
    <div className={styles.container}>
      <Head>
        <title>Pokedex example</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      {
        router.isReady && viewMode && <GalleryPage
          onSearchParamsChange={handleSearchParamsChange}
          onViewModeChange={handleViewModeChange}
          searchParams={parseQueryParams(router.query)}
          viewMode={viewMode}
        />
      }
    </div>
  )
}
