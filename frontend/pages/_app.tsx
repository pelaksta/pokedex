import "../styles/globals.css"

function Pokedex({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

export default Pokedex;
