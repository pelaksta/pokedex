import Head from "next/head";
import { useRouter } from "next/router";
import useDetails from "hooks/useDetails";
import LoadingOverlay from "components/LoadingOverlay";
import DetailsCard from "components/DetailsCard";
import Notification, { NotificationType } from "components/Notification";
import styles from "../../styles/Details.module.css";
import { useCallback, useEffect, useRef } from "react";
import useMutations from "hooks/useMutations";

export default function Home() {
  const router = useRouter();
  const notificationRef = useRef<Notification>();
  const { data, isLoading, isError, mutate } = useDetails(router.query.id);
  const { favorite, unfavorite, hasError } = useMutations(mutate);

  // Notification
  useEffect(() => {
    (isError || hasError) && notificationRef.current?.show(NotificationType.ERROR, "Fetching the data failed" );
  }, [isError, hasError]);

  const toggleFavorite = useCallback((id: string, toggle: boolean) => {
    toggle ? favorite(id) : unfavorite(id);
  }, [favorite, unfavorite]);

  return (
    <div className={styles.container}>
      <Head>
        <title>Pokedex example</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Notification ref={notificationRef} />

      { isLoading && <LoadingOverlay /> }

      { !isLoading && <DetailsCard item={data} onToggleFavorite={toggleFavorite} /> }
    </div>
  )
}
